package org.me;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

public class Encode3 {

	// Last byte of each item of Encode2.mostPopulars
	public static List<String> mostPopulars67 = Lists.newArrayList(
			"D2", "DA", "34", "C5", "6A", "B4", "29", "D5", "2C",
			"34", "C5", "6A", "B4", "29", "D5", "2C", "D3", "C5",
			"6A", "B4", "29", "D5", "2C", "D3", "91", "6A", "B4",
			"29", "D5", "2C", "D3", "91", "B3", "B4", "29", "D5",
			"2C", "D3", "91", "B3", "6C", "29", "D5", "2C", "D3",
			"91", "B3", "6C", "49", "D5", "2C", "D3", "91", "B3",
			"6C", "49", "52", "2C", "D3", "91", "B3", "6C", "49",
			"52", "9C", "D3", "91", "B3", "6C", "49", "52", "9C",
			"4D", "91", "B3", "6C", "49", "52", "9C", "4D", "65",
			"B3", "6C", "49", "52", "9C", "4D", "65", "C3", "6C",
			"49", "52", "9C", "4D", "65", "C3", "4A", "49", "52",
			"9C", "4D", "65", "C3", "4A", "5B", "52", "9C", "4D",
			"65", "C3", "4A", "5B", "D6", "9C", "4D", "65", "C3",
			"4A", "5B", "D6", "92", "4D", "65", "C3", "4A", "5B",
			"D6", "92", "65", "C3", "4A", "5B", "D6", "92", "6D",
			"94", "C3", "4A", "5B", "D6", "92", "6D", "94", "A6",
			"4A", "5B", "D6", "92", "6D", "94", "A6", "55", "5B",
			"D6", "92", "6D", "94", "A6", "55", "CD", "D6", "92",
			"6D", "94", "A6", "55", "CD", "2B", "92", "6D", "94",
			"A6", "55", "CD", "2B", "9A", "6D", "94", "A6", "55",
			"CD", "2B", "9A", "36", "94", "A6", "55", "CD", "2B",
			"9A", "36", "95", "A6", "55", "CD", "2B", "9A", "36",
			"95", "4B", "55", "CD", "2B", "9A", "36", "95", "4B",
			"D4", "CD", "2B", "9A", "36", "95", "4B", "D4", "35",
			"2B", "9A", "36", "95", "4B", "D4", "35", "8D", "9A",
			"36", "95", "4B", "D4", "35", "8D", "96", "36", "95",
			"4B", "D4", "35", "8D", "96", "B2", "95", "4B", "D4",
			"35", "8D", "96", "B2", "A3", "4B", "D4", "35", "8D",
			"96", "B2", "A3"
			);

	public static String mostPopular0(int i) {
		int v;
		if (i <= 127) {
			v = (i % 2 == 0) ? 0b0101 : 0b1010;
		} else {
			v = (i % 2 == 1) ? 0b0101 : 0b1010;
		}
		return String.format("%x", v).toUpperCase();
	}

	public static String mostPopulars1Cycle = "2C943B7965B7D68D";

	public static String mostPopular1(int i) {
		if (i <= 127) {
			return mostPopulars1Cycle.substring(i % 16, i % 16 + 1);
		} else {
			return mostPopulars1Cycle.substring((i + 1) % 16, (i + 1) % 16 + 1);
		}
	}

	public static String mostPopular45(int i) {
		if (i == 0) {
			return "A3";
		} else if (i == 1) {
			return "86";
		} else if (i <= 127) {
			if (i % 8 == 1) {
				return mostPopulars67.get(i - 8);
			} else {
				return mostPopulars67.get(i - 1);
			}
		} else if (i == 128) { // i !<= 127 but i - 8 < 127, and i % 8 == 0, hence these seems to be one shift in the index diff.
			return mostPopulars67.get(i - 7);
		} else {
			if (i % 8 == 0) {
				return mostPopulars67.get(i - 8);
			} else {
				return mostPopulars67.get(i - 1);
			}
		}
	}

	public static String mostPopular23(int i) {
		if (i == 0) {
			return "B2";
		} else if (i == 1) {
			return "CB";
		} else if (i <= 127) {
			if (i % 8 == 1) {
				return mostPopular45(i - 8);
			} else {
				return mostPopular45(i - 1);
			}
		} else if (i == 128) {
			return mostPopular45(i - 7);
		} else {
			if (i % 8 == 0) {
				return mostPopular45(i - 8);
			} else {
				return mostPopular45(i - 1);
			}
		}
	}

	public static String mostPopular(int i) {
		return mostPopular0(i) + mostPopular1(i) + mostPopular23(i) + mostPopular45(i) + mostPopulars67.get(i);
	}

	public static String secondMostPopular(int i) {
		if (0 <= i && i < 255) {
			if (i < 127) {
				return mostPopular(i + 1);
			} else if (i > 128) {
				return mostPopular(i - 1);
			} else {
				return "52D6926D";
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	// This thing encoded v2 reading with 30 irregular values.
	// More recent v3 reading changed 8 values, exactly to what were
	// suggested by this schema.
	public static Map<Integer, String> irregular = new HashMap<Integer, String>() {
		{
			//put(6, "7F000000");
			put(1131, "52D6926D");
			put(1287, "A586DA34");
			//put(1644, "6C7F0000");
			put(2842, "5329D52C");
			put(4668, "A66C4952");
			put(5311, "536C4952");
			//put(9196, "A0A00F1D");
			put(11594, "524A5BD6");
			put(13122, "534D65C3");
			put(13813, "A9D391B3");
			//put(19904, "000401FF");
			put(20655, "5249529C");
			put(22975, "A555CD2B");
			put(25007, "AC9A3695");
			put(25068, "58D391B3");
			//put(25451, "0000FFC7");
			put(28252, "A791B36C");
			put(33309, "53926D94");
			put(35364, "A7926D94");
			put(35765, "A72CD391");
			put(37731, "A9B429D5");
			put(40296, "5629D52C");
			//put(40932, "64640064");
			//put(41308, "64640064");
			put(43668, "AD2B9A36");
			put(46540, "A74D65C3");
			put(49868, "526D94A6");
			//put(57241, "00A0A00F");
			put(65535, "AD96B2A3");
		}
	};

	public static String fn1(int i) {
		if (0 <= i && i < 32768) {
			int block = i / 256;
			int secondSeqSize = 128 - block;
			boolean useMostPopular = i % 256 >= secondSeqSize;
			String v = irregular.get(i);
			if (v == null) {
				if (useMostPopular) {
					//v = mostPopulars.get(i % 255);
					v = mostPopular(i % 255);
				} else {
					// v = secondMostPopulars.get(i % 255);
					v = secondMostPopular(i % 255);
				}
			}
			return v;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static String fn2(int i) {
		if (32768 <= i && i < 65536) {
			int block = i / 256;
			int secondSeqSize = block - 127;
			boolean useMostPopular = 255 - i % 256 >= secondSeqSize;
			String v = irregular.get(i);
			if (v == null) {
				if (useMostPopular) {
					//v = mostPopulars.get(i % 255);
					v = mostPopular(i % 255);
				} else {
					// v = secondMostPopulars.get(i % 255);
					v = secondMostPopular(i % 255);
				}
			}
			return v;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static List<String> generateSeq() {
		List<String> r = new ArrayList<>();
		for (int i = 0; i < 32768; ++i) {
			String v = fn1(i);
			r.add(v);
			//System.out.println(i + " " + r.get(v));
		}
		for (int i = 32768; i < 65536; ++i) {
			String v = fn2(i);
			r.add(v);
			//System.out.println(i + " " + r.get(v));
		}
		return r;
	}
}
