package org.me;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

public class Encode2 {

	// Item at index i of this seq is the most popular value at index 255 * j + i of the original seq.
	// Hence, if periodicity with period length 255 is assumed, then these are good candidates for the periodic values.
	public static List<String> mostPopulars = Lists.newArrayList(
			"52B2A3D2", "ACCB86DA", "5986DA34", "A4DA34C5", "5334C56A", "ABC56AB4", "576AB429", "A9B429D5", "5629D52C",
			"A586DA34", "5BDA34C5", "A734C56A", "5DC56AB4", "A66AB429", "58B429D5", "AD29D52C", "52D52CD3", "ACDA34C5",
			"5934C56A", "A4C56AB4", "536AB429", "ABB429D5", "5729D52C", "A9D52CD3", "562CD391", "A534C56A", "5BC56AB4",
			"A76AB429", "5DB429D5", "A629D52C", "58D52CD3", "AD2CD391", "52D391B3", "ACC56AB4", "596AB429", "A4B429D5",
			"5329D52C", "ABD52CD3", "572CD391", "A9D391B3", "5691B36C", "A56AB429", "5BB429D5", "A729D52C", "5DD52CD3",
			"A62CD391", "58D391B3", "AD91B36C", "52B36C49", "ACB429D5", "5929D52C", "A4D52CD3", "532CD391", "ABD391B3",
			"5791B36C", "A9B36C49", "566C4952", "A529D52C", "5BD52CD3", "A72CD391", "5DD391B3", "A691B36C", "58B36C49",
			"AD6C4952", "5249529C", "ACD52CD3", "592CD391", "A4D391B3", "5391B36C", "ABB36C49", "576C4952", "A949529C",
			"56529C4D", "A52CD391", "5BD391B3", "A791B36C", "5DB36C49", "A66C4952", "5849529C", "AD529C4D", "529C4D65",
			"ACD391B3", "5991B36C", "A4B36C49", "536C4952", "AB49529C", "57529C4D", "A99C4D65", "564D65C3", "A591B36C",
			"5BB36C49", "A76C4952", "5D49529C", "A6529C4D", "589C4D65", "AD4D65C3", "5265C34A", "ACB36C49", "596C4952",
			"A449529C", "53529C4D", "AB9C4D65", "574D65C3", "A965C34A", "56C34A5B", "A56C4952", "5B49529C", "A7529C4D",
			"5D9C4D65", "A64D65C3", "5865C34A", "ADC34A5B", "524A5BD6", "AC49529C", "59529C4D", "A49C4D65", "534D65C3",
			"AB65C34A", "57C34A5B", "A94A5BD6", "565BD692", "A5529C4D", "5B9C4D65", "A74D65C3", "5D65C34A", "A6C34A5B",
			"584A5BD6", "AD5BD692", "AC9C4D65", "594D65C3", "A465C34A", "53C34A5B", "AB4A5BD6", "575BD692", "A9D6926D",
			"56926D94", "A54D65C3", "5B65C34A", "A7C34A5B", "5D4A5BD6", "A65BD692", "58D6926D", "AD926D94", "526D94A6",
			"AC65C34A", "59C34A5B", "A44A5BD6", "535BD692", "ABD6926D", "57926D94", "A96D94A6", "5694A655", "A5C34A5B",
			"5B4A5BD6", "A75BD692", "5DD6926D", "A6926D94", "586D94A6", "AD94A655", "52A655CD", "AC4A5BD6", "595BD692",
			"A4D6926D", "53926D94", "AB6D94A6", "5794A655", "A9A655CD", "5655CD2B", "A55BD692", "5BD6926D", "A7926D94",
			"5D6D94A6", "A694A655", "58A655CD", "AD55CD2B", "52CD2B9A", "ACD6926D", "59926D94", "A46D94A6", "5394A655",
			"ABA655CD", "5755CD2B", "A9CD2B9A", "562B9A36", "A5926D94", "5B6D94A6", "A794A655", "5DA655CD", "A655CD2B",
			"58CD2B9A", "AD2B9A36", "529A3695", "AC6D94A6", "5994A655", "A4A655CD", "5355CD2B", "ABCD2B9A", "572B9A36",
			"A99A3695", "5636954B", "A594A655", "5BA655CD", "A755CD2B", "5DCD2B9A", "A62B9A36", "589A3695", "AD36954B",
			"52954BD4", "ACA655CD", "5955CD2B", "A4CD2B9A", "532B9A36", "AB9A3695", "5736954B", "A9954BD4", "564BD435",
			"A555CD2B", "5BCD2B9A", "A72B9A36", "5D9A3695", "A636954B", "58954BD4", "AD4BD435", "52D4358D", "ACCD2B9A",
			"592B9A36", "A49A3695", "5336954B", "AB954BD4", "574BD435", "A9D4358D", "56358D96", "A52B9A36", "5B9A3695",
			"A736954B", "5D954BD4", "A64BD435", "58D4358D", "AD358D96", "528D96B2", "AC9A3695", "5936954B", "A4954BD4",
			"534BD435", "ABD4358D", "57358D96", "A98D96B2", "5696B2A3", "A536954B", "5B954BD4", "A74BD435", "5DD4358D",
			"A6358D96", "588D96B2", "AD96B2A3"
			);

	public static String secondMostPopular(int i) {
		if (0 <= i && i < 255) {
			if (i < 127) {
				return mostPopulars.get(i + 1);
			} else if (i > 128) {
				return mostPopulars.get(i - 1);
			} else {
				return "52D6926D";
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	// This thing encoded v2 reading with 30 irregular values.
	// More recent v3 reading changed 8 values, exactly to what were
	// suggested by this schema.
	public static Map<Integer, String> irregular = new HashMap<Integer, String>() {
		{
			//put(6, "7F000000");
			put(1131, "52D6926D");
			put(1287, "A586DA34");
			//put(1644, "6C7F0000");
			put(2842, "5329D52C");
			put(4668, "A66C4952");
			put(5311, "536C4952");
			//put(9196, "A0A00F1D");
			put(11594, "524A5BD6");
			put(13122, "534D65C3");
			put(13813, "A9D391B3");
			//put(19904, "000401FF");
			put(20655, "5249529C");
			put(22975, "A555CD2B");
			put(25007, "AC9A3695");
			put(25068, "58D391B3");
			//put(25451, "0000FFC7");
			put(28252, "A791B36C");
			put(33309, "53926D94");
			put(35364, "A7926D94");
			put(35765, "A72CD391");
			put(37731, "A9B429D5");
			put(40296, "5629D52C");
			//put(40932, "64640064");
			//put(41308, "64640064");
			put(43668, "AD2B9A36");
			put(46540, "A74D65C3");
			put(49868, "526D94A6");
			//put(57241, "00A0A00F");
			put(65535, "AD96B2A3");
		}
	};

	public static String fn1(int i) {
		if (0 <= i && i < 32768) {
			int block = i / 256;
			int secondSeqSize = 128 - block;
			boolean useMostPopular = i % 256 >= secondSeqSize;
			String v = irregular.get(i);
			if (v == null) {
				if (useMostPopular) {
					v = mostPopulars.get(i % 255);
				} else {
					v = secondMostPopular(i % 255);
				}
			}
			return v;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static String fn2(int i) {
		if (32768 <= i && i < 65536) {
			int block = i / 256;
			int secondSeqSize = block - 127;
			boolean useMostPopular = 255 - i % 256 >= secondSeqSize;
			String v = irregular.get(i);
			if (v == null) {
				if (useMostPopular) {
					v = mostPopulars.get(i % 255);
				} else {
					v = secondMostPopular(i % 255);
				}
			}
			return v;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static List<String> generateSeq() {
		List<String> r = new ArrayList<>();
		for (int i = 0; i < 32768; ++i) {
			String v = fn1(i);
			r.add(v);
			//System.out.println(i + " " + r.get(v));
		}
		for (int i = 32768; i < 65536; ++i) {
			String v = fn2(i);
			r.add(v);
			//System.out.println(i + " " + r.get(v));
		}
		return r;
	}
}
