package org.me;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.Set;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

public class App {

	public static class Pair {

		public String k, v;

		public Pair(String k, String v) {
			this.k = k;
			this.v = v;
		}
	}

	public static List<Pair> read() throws IOException, URISyntaxException {
		//InputStream is = App.class.getResourceAsStream("bugs3_rxid_radioid_v3.txt");
		//File f = new File("/home/marci/bugs3-1/bugs3_rxid_radioid_v3.txt");
		File f = Paths.get(App.class.getClassLoader().getResource("bugs3_rxid_radioid_v3.txt").toURI()).toFile();
		List<String> lines = Files.readLines(f, Charsets.UTF_8);
		System.out.println("All pairs: " + lines.size());
		List<Pair> r = new ArrayList<>();
		for (String s : lines) {
			String[] ss = s.split(",");
			r.add(new Pair(ss[0], ss[1]));
		}
		return r;
	}

	public static void simpleStats(List<Pair> pairs) {
		Set<String> values = new HashSet<>();
		for (int i = 0; i < pairs.size(); ++i) {
			values.add(pairs.get(i).v);
		}
		System.out.println("Different values: " + values.size());
	}

	public static void checkPeriodicityOfFirst(List<Pair> pairs, int period) {
		List<Pair> first255Pairs = pairs.subList(0, period);
		System.out.println("First" + period + " pairs: " + first255Pairs.size());
		int first255EqualsCount = 0;
		int first255DiffCount = 0;
		for (int i = 0; i < pairs.size(); ++i) {
			String value = pairs.get(i).v;
			String first255Value = first255Pairs.get(i % period).v;
			if (value.equals(first255Value)) {
				++first255EqualsCount;
			} else {
				++first255DiffCount;
			}
		}
		System.out.println("First" + period + " match: " + first255EqualsCount);
		System.out.println("First" + period + " diff: " + first255DiffCount);
	}

	public static List<Entry<String, Integer>> sortByValues(Map<String, Integer> map) {
		List<Entry<String, Integer>> l = new ArrayList<>(map.entrySet());
		Comparator<Entry<String, Integer>> c = Entry.comparingByValue();
		l.sort(c.reversed());
		return l;
	}

	public static Map<Integer, Map<String, Integer>> collectPopularValues(List<Pair> pairs, int period) {
		Map<Integer, Map<String, Integer>> popularValues = new HashMap<>();
		for (int i = 0; i < pairs.size(); ++i) {
			String value = pairs.get(i).v;
			Map<String, Integer> tmp1 = popularValues.get(i % period);
			if (tmp1 == null) {
				tmp1 = new HashMap<>();
				popularValues.put(i % period, tmp1);
			}
			Integer tmp2 = tmp1.get(value);
			if (tmp2 == null) {
				tmp1.put(value, 1);
			} else {
				tmp1.put(value, tmp2 + 1);
			}
		}
		System.out.println("Popular values: " + popularValues.size());
		for (int i = 0; i < period; ++i) {
			System.out.println(i + " " + sortByValues(popularValues.get(i)));
		}
		return popularValues;
	}

	public static void checkPeriodicityOfFirstAndSecondPopular(List<Pair> pairs, int period) {
		Map<Integer, Map<String, Integer>> popularValues = collectPopularValues(pairs, period);
		int mostPopular1Match = 0;
		int mostPopular2Match = 0;
		//int mostPopular3Match = 0;
		Map<Integer, String> mostPopular1Map = new HashMap<Integer, String>();
		Map<Integer, String> mostPopular2Map = new HashMap<Integer, String>();
		//Map<Integer, String> mostPopular3Map = new HashMap<Integer, String>();
		for (Entry<Integer, Map<String, Integer>> e : popularValues.entrySet()) {
			List<Entry<String, Integer>> r = sortByValues(e.getValue());
			//System.out.println(e.getKey() + " " + r.get(0));
			mostPopular1Match += r.get(0).getValue();
			mostPopular1Map.put(e.getKey(), r.get(0).getKey());
			if (r.size() >= 2) {
				mostPopular2Match += r.get(1).getValue();
				mostPopular2Map.put(e.getKey(), r.get(1).getKey());
			}
			//if (l1.size() >= 3) {
			//	mostPopular3Match += l1.get(2).getValue();
			//}
		}
		System.out.println("First most popular match: " + mostPopular1Match);
		System.out.println("Second most popular match: " + mostPopular2Match);
		int count1 = 0;
		int count2 = 0;
		Boolean firstMatch = null;
		int change = 0;
		int irregular = 0;
		int lastChange = 0;
		for (int i = 0; i < pairs.size(); ++i) {
			Pair p = pairs.get(i);
			String v1 = mostPopular1Map.get(i % period);
			if (v1.equals(p.v)) {
				++count1;
				if (firstMatch == null || firstMatch == false) {
					++change;
					System.out.println("Change to first: " + i + " diff: " + (i - lastChange));
					lastChange = i;
				}
				firstMatch = true;
			} else {
				String v2 = mostPopular2Map.get(i % period);
				if (v2 != null && v2.equals(p.v)) {
					++count2;
					if (firstMatch == null || firstMatch == true) {
						++change;
						System.out.println("Change to second:" + i + " diff: " + (i - lastChange));
						lastChange = i;
					}
					firstMatch = false;
				} else {
					++irregular;
					System.out.println("Irregular " + i + " " + irregular);
				}
			}
		}
		System.out.println("Most popular " + count1);
		System.out.println("Most popular " + count2);
		System.out.println("Irregular " + irregular);
		System.out.println("Change " + change);
		//for (int i = 0; i < 255; ++i) {
		//	if (i != 254) {
		//		System.out.println("\"" + mostPopular1Map.get(i) + "\",");
		//	} else {
		//		System.out.println("\"" + mostPopular1Map.get(i) + "\"");
		//	}
		//}
		//for (int i = 0; i < 255; ++i) {
		//	if (i != 254) {
		//		System.out.println("\"" + mostPopular2Map.get(i) + "\",");
		//	} else {
		//		System.out.println("\"" + mostPopular2Map.get(i) + "\"");
		//	}
		//}
	}

	public static <T> void check(List<T> a, List<T> b) {
		int match = 0;
		int diff = 0;
		for (int i = 0; i < Math.min(a.size(), b. size()); ++i) {
			if (a.get(i).equals(b.get(i))) {
				++match;
			} else {
				++diff;
			}
		}
		diff += Math.abs(a.size() - b.size());
		System.out.println("match: " + match + " diff: " + diff);
	}

	public static void check2345vs67(List<Pair> pairs) {
		int ok1 = 0; // byte 1 to byte 2 of prev
		int ok2 = 0; // byte 2 to byte 3 of prev
		int ok3 = 0; // byte 3 to nothing
		int diff1 = 0;
		int diff2 = 0;
		int diff12 = 0;
		int diff3 = 0;
		for (int i = 0; i < Encode1.mostPopulars.size(); ++i) {
			if (i == 0) {
				++diff1;
				++diff2;
				System.out.println(i);
				System.out.println(i);
				++diff3;
			} else {
				boolean match1 = Encode1.mostPopulars.get(i).substring(2, 4).equals(Encode1.mostPopulars.get(i - 1).substring(4, 6));
				if (match1) {
					++ok1;
				} else {
					++diff1;
					System.out.println(i);
				}
				boolean match2 = Encode1.mostPopulars.get(i).substring(4, 6).equals(Encode1.mostPopulars.get(i - 1).substring(6, 8));
				if (match2) {
					++ok2;
				} else {
					++diff2;
					System.out.println(i);
				}
				if (!(match1 && match2)) {
					++diff12;
				}
				++diff3;
			}
		}
		System.out.println(ok1 + " " + ok2 + " " + ok3);
		System.out.println(diff1 + " " + diff2 + " " + diff3);
		System.out.println(diff12);
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		List<Pair> pairs = App.read();
		simpleStats(pairs);
		checkPeriodicityOfFirst(pairs, 255);
		checkPeriodicityOfFirstAndSecondPopular(pairs, 255);
		{
			List<String> generated = Encode1.generateSeq();
			check(pairs.stream().map(p -> p.v).collect(Collectors.toList()),
					generated);
		}
		{
			List<String> generated = Encode2.generateSeq();
			check(pairs.stream().map(p -> p.v).collect(Collectors.toList()),
					generated);
		}
		check2345vs67(pairs);
		{
			List<String> generated = Encode3.generateSeq();
			check(pairs.stream().map(p -> p.v).collect(Collectors.toList()),
					generated);
		}
		//for (int i = 0; i < 0/*pairs.size()*/; ++i) {
		////for (int i = 8416; i < 8416 + 128; ++i) {
		//	Pair p = pairs.get(i);
		//	String idLast2 = p.k.substring(2, 4);
		//	int idLast2i = Integer.decode("0x" + idLast2);
		//	String last2 = p.v.substring(6, 8);
		//	int last2i = Integer.decode("0x" + last2);
		//	System.out.println(
		//			p.k + ", " + idLast2 + ", " + String.format("%3d", idLast2i) + ", " +
		//			String.format("%16s", Integer.toBinaryString(idLast2i)).replace(' ', '0') + ", " +
		//			p.v + ", " + last2 + ", " + String.format("%3d", last2i) + ", " +
		//			String.format("%16s", Integer.toBinaryString(last2i)).replace(' ', '0')
		//			//Integer.toBinaryString(last2i)
		//			);
		//}
		//Set<String> ss = new HashSet(Encode3.mostPopularsDigit67);
		//System.out.println(ss.size());
		//Map<String, Integer> steps = new HashMap<>();
		//for (int i = 0; i < Encode3.mostPopulars67.size() - 1; ++i) {
		//	String k0 = Encode3.mostPopulars67.get(i);
		//	//Long k0l = Long.decode("0x" + k0);
		//	Long k01 = Long.decode("0x" + k0.substring(0, 1));
		//	Long k02 = Long.decode("0x" + k0.substring(1, 2));
		//	System.out.println(String.format("%3d", i) + " " +
		//			String.format("%8s", Long.toBinaryString(i)).replace(' ', '0') + " " +
		//			k0 + " " +
		//			k0.substring(0, 1) + " " + k0.substring(1, 2) + " " +
		//			String.format("%4s", Long.toBinaryString(k01)).replace(' ', '0') + " " +
		//			String.format("%4s", Long.toBinaryString(k02)).replace(' ', '0'));
		//	String k1 = Encode3.mostPopularsDigit67.get(i + 1);
		//	Long k11 = Long.decode("0x" + k1.substring(0, 1));
		//	Long k12 = Long.decode("0x" + k1.substring(1, 2));
		//	String k = k0 + " " + k1;
		//	Integer c = steps.get(k);
		//	if (c == null) {
		//		steps.put(k, 1);
		//	} else {
		//		steps.put(k, c + 1);
		//	}
		//}
		System.out.println(new HashSet<>(Encode3.mostPopulars67).size());
		{
			// 2 indices between each,
			// finishes exactly at 127
			List<Integer> starts = Arrays.asList(2, 18, 34, 50, 66, 82, 98, 114);
			for (Integer start : starts) {
				System.out.println("start " + start);
				List<Pair> pairs67 = new ArrayList<>();
				for (int i = start; i < start + 14; ++i) {
					String k0 = Encode3.mostPopulars67.get(i);
					pairs67.add(new Pair("" + i, k0));
				}
				Map<Integer, Map<String, Integer>> popular67 = collectPopularValues(pairs67, 7);
				for (Entry<Integer, Map<String, Integer>> e : popular67.entrySet()) {
					System.out.println(e.getKey() + " " + e.getValue().size());
				}
			}
		}
		{
			// 2 indices between each, only one at the beginning,
			// finishes exactly at 254
			List<Integer> starts = Arrays.asList(129, 145, 161, 177, 193, 209, 225, 241);
			for (Integer start : starts) {
				System.out.println("start " + start);
				List<Pair> pairs67 = new ArrayList<>();
				for (int i = start; i < start + 14; ++i) {
					String k0 = Encode3.mostPopulars67.get(i);
					pairs67.add(new Pair("" + i, k0));
				}
				Map<Integer, Map<String, Integer>> popular67 = collectPopularValues(pairs67, 7);
				for (Entry<Integer, Map<String, Integer>> e : popular67.entrySet()) {
					System.out.println(e.getKey() + " " + e.getValue().size());
				}
			}
		}
		//System.out.println(pairs67);
		//System.out.println(steps);
		//System.out.println(steps.size());
		{
			List<String> generated = Encode4.generateSeq();
			check(pairs.stream().map(p -> p.v).collect(Collectors.toList()),
					generated);
		}
		{
			List<String> generated = Encode5.generateSeq();
			check(pairs.stream().map(p -> p.v).collect(Collectors.toList()),
					generated);
		}
	}
}
