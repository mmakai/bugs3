#include <cstdio>
#include <iomanip>
#include <iostream>
extern "C" {
#include "bugs3_rxid_radioid_map.h"
}

int main() {
  for (int i = 0; i < 65536; ++i) {
    std::cout << std::setfill('0') << std::setw(4) << i << "," << std::hex << std::uppercase << rxid_to_radioid(i) << std::endl;
  }
  return 0;
}
